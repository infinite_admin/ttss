<?php
require_once __DIR__ . '/../vendor/autoload.php';
use Workerman\Worker;


// Create a Websocket server 
$ws_worker = new Worker("websocket://localhost:53");

// 4 processes
$ws_worker->count = 4; // не совсем понятно что именно делает этот параметр

$ws_worker->onWorkerStart = function($worker)
{
  echo "Worker starting...\n";
};

// Emitted when new connection come
$ws_worker->onConnect = function($connection)
{
  echo "New connection\n";
  echo "-------: ". print_r($connection::$statistics)."\n";
};

// Emitted when data received
$ws_worker->onMessage = function($connection, $data) use ($ws_worker)
{
  // Send hello $data
//  $connection->send('hello ' . $data);

  foreach($ws_worker->connections as $conn)
  {
    $conn->send($data);
  }


//  $connection->send($data);
  echo 'hello ' . $data . "\n";
};

// Emitted when connection closed
$ws_worker->onClose = function($connection)
{
  echo "Connection closed\n";
};

// Run worker
Worker::runAll();